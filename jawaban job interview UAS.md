# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
- Use case user:

| Usecase Gmail | Nilai |
| ------ | ------ |
|Mengirim Email        |  90  |
|Menampilkan Email     |  75   |
|Mencari Email | 75 |
|Menghapus Email | 70 |
|Mengedit Pesan | 80 |
|Notifikasi | 50 |


- Use case manajemen perusahaan

| Usecase Gmail | Nilai |
| ------ | ------ |
|Mengedit Akun User       |  95  |
|Mengelola Akses     |  90   |

- Use case direksi perusahaan (dashboard, monitoring, analisis)

| Usecase Gmail | Nilai |
| ------ | ------ |
|Mengedit Akun User       |  95  |
|Mengirim Email        |  90  |
|Menampilkan Email     |  75   |
|Mencari Email | 75 |
|Menghapus Email | 70 |
|Mengedit Pesan | 80 |
|Notifikasi | 50 |

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

Class diagram menggunakan mermaid



```mermaid
classDiagram
    class Message {
        # subject: String
        # body: String
        + Message(subject: String, body: String)
        + getSubject(): String
        + getBody(): String
        + abstract getRecipient(): String
    }

    class Email {
        - to: String
        - from: String
        + Email(to: String, from: String, subject: String, body: String)
        + getTo(): String
        + getFrom(): String
        + getRecipient(): String
    }

    class EmailApp {
        - inbox: ArrayList<Email>
        - sent: ArrayList<Email>
        - scanner: Scanner
        - loggedIn: boolean
        - loggedInUsername: String
        + EmailApp()
        + menu()
        + login()
        + MengirimEmail()
        + MenampilkanEmailMasuk()
        + MenampilkanEmailTerkirim()
        + MenghapusEmail()
        + MencariEmail()
        + getEmailFromUsername(username: String): String
    }

    EmailApp o-- Email
    EmailApp o-- Scanner
    EmailApp --> ArrayList
    Email --|> Message

```

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

- Single Responsibility Principle (SRP): Prinsip SRP diterapkan pada kelas EmailContainer. Kelas EmailContainer bertanggung jawab untuk merender konten email. Fungsi render() pada kelas ini hanya fokus pada tugasnya yang menghasilkan tampilan konten email.

```
// Single Responsibility Principle (SRP)
class EmailContainer implements EmailRendererInterface
{
    public function render(): string
    {
        return "
            <div class='container'>
                <div class='email-header'>
                    <h2>Selamat Datang Gmail!</h2>
                </div>
                <div class='email-body'>
                    <p>Halo,</p>
                    <p>Ini adalah email dengan tampilan seperti email yang dikirimkan kepada pengguna.</p>
                    <p></p>
                    <p>Salam,</p>
                    <p>Ardian Malik Muharam</p>
                </div>
                <div class='email-footer'>
                    <p>Silakan pilih salah satu layanan di navbar di atas.</p>
                </div>
            </div>
        ";
    }
}
```

- Open/Closed Principle (OCP):
Kode telah diubah untuk menggunakan konsep dependency injection.
EmailRendererFactory berfungsi sebagai factory yang menghasilkan instance dari EmailContainer.
Dengan demikian, ketika ingin menambahkan jenis email baru, cukup menambahkan implementasi baru dari EmailRendererInterface tanpa mengubah kelas-kelas yang sudah ada.

```
// Open/Closed Principle (OCP)
// Factory untuk EmailRenderer
class EmailRendererFactory
{
    public static function createEmailRenderer(): EmailRendererInterface
    {
        return new EmailContainer();
    }
}
```

- Interface Segregation Principle (ISP):
EmailRendererInterface hanya mendefinisikan satu metode render(), sesuai dengan prinsip bahwa klien tidak boleh dipaksa untuk mengimplementasikan metode yang tidak digunakan.

```
//Interface Segregation Principle (ISP)
// Interface untuk kelas EmailRenderer
interface EmailRendererInterface
{
    public function render(): string;
}
```

- Dependency Inversion Principle (DIP):
Dalam route handler, instance EmailRendererInterface diambil dari container menggunakan $this->get('EmailRenderer'). 
Penggunaan dependency inversion memungkinkan kelas-kelas tingkat tinggi (seperti route handler) untuk bergantung pada abstraksi EmailRendererInterface daripada implementasi khusus EmailContainer. Hal ini memungkinkan fleksibilitas
dalam mengganti implementasi email renderer di masa depan tanpa merusak kelas-kelas yang bergantung pada abstraksi tersebut.

```
$app->get('/', function (Request $request, Response $response) {
    $emailRenderer = $this->get('EmailRenderer');
    $content = $emailRenderer->render();
```




# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih


**1. Singleton Pattern:**
Design pattern Singleton digunakan untuk memastikan bahwa hanya ada satu instance dari sebuah kelas yang dapat diakses secara global.

Dalam kode tersebut, terdapat penggunaan Singleton pada kelas MessageSender. Singleton digunakan untuk memastikan bahwa hanya ada satu objek MessageSender yang digunakan untuk mengirim pesan 

```
<?php
class MessageSender
{
    private static $instance; // Menyimpan instance tunggal

    private function __construct()
    {
        // Private constructor untuk mencegah instansiasi langsung
    }

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function sendMessage($id, $pengirim, $pesan)
    {
        // Logika pengiriman pesan
        // ...
    }
}

// Menggunakan singleton instance
$messageSender = MessageSender::getInstance();

?>
```

**2. Front Controller Pattern:**
Pola desain yang digunakan untuk memusatkan pemrosesan permintaan pada satu titik masuk utama (front controller) dalam aplikasi.

Dalam kodingan yang Anda berikan, terdapat implementasi Front Controller Pattern dengan menggunakan file index.php sebagai front controller. 

```
<?php
// Front Controller
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['action'])) {
        $action = $_POST['action'];

        switch ($action) {
            case 'login':
                include 'p_log.php';
                break;
            default:
                echo "Tindakan tidak valid";
                break;
        }
    }
}
?>
```


# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

![dokumentasi jawaban uas](dokumentasi jawaban uas/no5.jpg)

Kode di atas melakukan otentikasi pengguna dengan memeriksa username dan password yang diberikan melalui formulir login dengan data yang ada di tabel "user" dalam database. 

Dengan menambahkan langkah-langkah di atas,akan memiliki koneksi database yang aktif yang dapat digunakan untuk menjalankan kueri otentikasi dan memeriksa apakah pengguna berhasil masuk atau tida


# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- **Web service yang dipakai**

Slim FrameWork V3

- **Web Page Untuk User**
![dokumentasi jawaban uas](dokumentasi jawaban uas/no6_tampilan.jpg)


- **Web Service Untuk Aplikasi**
![dokumentasi jawaban uas](dokumentasi jawaban uas/no6_webservice.jpg)


- **CRUD**

CRUD disini berfungsi untuk membuat pesan,memlihat pesan,meng-update pesan,dan mendelete pesan.

1. Creat

User dapat membuat pesan baru dengan cara klik navbar `Buat Pesan` lalu akan di proses di buat_pesan.php

```
<?php
class MessageSender
{
    private static $instance; // Menyimpan instance tunggal

    private function __construct()
    {
        // Private constructor untuk mencegah instansiasi langsung
    }

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function sendMessage($id, $pengirim, $pesan)
    {
        // Logika pengiriman pesan
        // ...
    }
}

// Menggunakan singleton instance
$messageSender = MessageSender::getInstance();

?>
```
![dokumentasi jawaban uas](dokumentasi jawaban uas/create.png)

2. Read 

User dapat melihat pesan yang terkirim dari web di file tampilan_pesan.php

```
<!DOCTYPE html>
<html>
<head>
    <title>Aplikasi Email</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>Menampilkan Pesan Terkirim</h2>

        <?php
        require_once "koneksi.php";

        // Ambil semua pesan dari database
        $sql = "SELECT * FROM pesans";
        $result = $conn->query($sql);

        // Tampilkan pesan dalam bentuk tabel
        if ($result->num_rows > 0) {
            echo '<table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Pengirim</th>
                            <th scope="col">Pesan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>';

            while ($row = $result->fetch_assoc()) {
                echo '<tr>
                        <td>' . $row["pengirim"] . '</td>
                        <td>' . $row["pesan"] . '</td>
                        <td>
                            <a href="edit_pesan.php?id=' . $row["id"] . '" class="btn btn-primary btn-sm">Edit</a>
                        </td>
                    </tr>';
            }

            echo '</tbody>
                </table>';
        } else {
            echo '<p>Tidak ada pesan yang tersedia.</p>';
        }

        $conn->close();
        ?>

        <a class="btn btn-primary" href="index.php">Kembali Ke Halaman Utama</a>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
```

![dokumentasi jawaban uas](dokumentasi jawaban uas/read.png)

3. Update

User dapat melihat pesan yang terkirim dari web di file tampilan_pesan.php dan user dapat mengedit pesan jika user merasa pesannya belum sesuai dan akan di proses di file edit_pesan.php

```
<?php
require_once "koneksi.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Ambil nilai dari form
    $id = $_POST["id"];
    $pengirim = $_POST["pengirim"];
    $pesan = $_POST["pesan"];

    // Update data pesan di dalam database
    $sql = "UPDATE pesans SET pengirim='$pengirim', pesan='$pesan' WHERE id=$id";

    if ($conn->query($sql) === TRUE) {
        echo "Pesan berhasil diupdate.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Ambil data pesan berdasarkan ID
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Query untuk mendapatkan pesan dari database
    $sql = "SELECT * FROM pesans WHERE id=$id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $pengirim = $row["pengirim"];
        $pesan = $row["pesan"];
    } else {
        echo "Pesan tidak ditemukan.";
        exit();
    }
} else {
    exit();
}

$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Aplikasi Email - Edit Pesan</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        .button-container {
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Edit Pesan</h2>
        <form id="editMessageForm" method="post" action="edit_pesan.php">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="form-group">
                <label for="pengirim">Pengirim:</label>
                <input type="text" class="form-control" id="pengirim" name="pengirim" value="<?php echo $pengirim; ?>" required>
            </div>
            <div class="form-group">
                <label for="pesan">Pesan:</label>
                <textarea class="form-control" id="pesan" name="pesan" rows="5" required><?php echo $pesan; ?></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <div class="button-container">
            <a class="btn btn-primary" href="index.php">Kembali Ke Halaman Utama</a>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
```

![dokumentasi jawaban uas](dokumentasi jawaban uas/update.png)

4. Delete

User dapat melakukan aksi delete pesan jika merasa pesan tersebut tidak dibutuhkan lagi dan akan diproses di hapus_pesan.php


```
<?php
require_once "koneksi.php";

// Hapus pesan berdasarkan ID
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Query untuk menghapus pesan dari database
    $sql = "DELETE FROM pesans WHERE id = $id";

    if ($conn->query($sql) === TRUE) {
        echo "Pesan berhasil dihapus.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Ambil data pesan dari database
$sql = "SELECT * FROM pesans";
$result = $conn->query($sql);
$messages = [];

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $messages[] = $row;
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Aplikasi Pesan - Hapus Pesan</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>Histori Pesan</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Pengirim</th>
                    <th>Pesan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($messages as $message): ?>
                    <tr>
                        <td><?php echo $message['pengirim']; ?></td>
                        <td><?php echo $message['pesan']; ?></td>
                        <td>
                            <a href="hapus_pesan.php?id=<?php echo $message['id']; ?>" class="btn btn-danger btn-sm">Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <div class="mt-4">
            <a class="btn btn-primary" href="index.php">Kembali ke Halaman Utama</a>
        </div>
    </div>
</body>
</html>
```

![dokumentasi jawaban uas](dokumentasi jawaban uas/delete.png)


# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

- Menu Navigasi

Terdapat menu navigasi di bagian atas yang memungkinkan user / pengguna untuk beralih ke fitur yang berbeda.


- Konten Utama

Bagian ini merupakan area halaman utama yang menampilkan informasi atau fungsi utama produk, seperti fitur buat pesan, menampilkan pesan, mencari pesan dan menghapus pesan.

- Header

Header berisi judul atau logo produk yang mengidentifikasikan halaman utama atau fitur yang sedang digunakan.


- Tombol dan Tautan

GUI juga menyediakan tombol dan tautan interaktif yang memungkinkan pengguna untuk melakukan tindakan tertentu, seperti mengklik tombol "kirim" pada menu membuat pesan untuk mengirim pesan.
![dokumentasi jawaban uas](dokumentasi jawaban uas/no6_tampilan.jpg)

![dokumentasi jawaban uas](dokumentasi jawaban uas/no6_tampilan1.jpg)



# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

- URL Input:Memasukkan URL tujuan ke dalam bidang input ini. URL ini mewakili alamat tujuan untuk melakukan koneksi HTTP,"http://localhost/iniardian/login.php".


- HTTP Methods: Terdapat pilihan metode HTTP yang tersedia, seperti GET, POST.


- Headers: Pengguna dapat menambahkan header tambahan yang akan dikirimkan dalam koneksi HTTP. Header ini berisi informasi tambahan yang dapat digunakan oleh server untuk memproses permintaan dengan benar.

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/jIWMaDy6FFc

