# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

- **use case** : user bisa melihat banyaknya pesan masuk dan terkirim

```
private void MenampilkanEmailMasuk() {
        if (inbox.size() == 0) {
            System.out.println("Tidak ada email masuk.");
        } else {
            for (Email email : inbox) {
                System.out.println("Dari: " + email.getFrom());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }

    private void MenampilkanEmailTerkirim() {
        if (sent.size() == 0) {
            System.out.println("Tidak ada email terkirim.");
        } else {
            for (Email email : sent) {
                System.out.println("Ke: " + email.getTo());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }
```
`if (inbox.size() == 0)` `if (sent.size() == 0)` yang merupakan bagian dalam kelas EmailApp

Pada kelas EmailApp, banyaknya pesan masuk dan terkirim dapat dihitung dengan mudah yaitu hanya dengan mengambil nilai atribut size.

# 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

- **use case** : user bisa mengirim email, melihat email masuk dan terkirim, menghapus semua email, dan mencari email berdasarkan subjek. 

1. Buatlah kelas Message sebagai kelas abstrak dengan atribut subject dan body yang merepresentasikan subjek dan isi pesan.


```
abstract class Message {
    protected String subject;
    protected String body;

    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public abstract String getRecipient();
}

```

2. Buatlah kelas Email sebagai subkelas dari Message dengan tambahan atribut to dan from yang merepresentasikan alamat email penerima dan pengirim. lalu Implementasikan konstruktor pada kelas Email untuk menginisialisasi atribut to, from, subject, dan body. Dalam konstruktor, panggil konstruktor superclass Message menggunakan kata kunci super. Kemudian Implementasikan metode getTo(), getFrom(), dan getRecipient() untuk mengakses nilai atribut dan mendapatkan alamat email penerima.



```
class Email extends Message {
    private String to;
    private String from;

    public Email(String to, String from, String subject, String body) {
        super(subject, body);
        this.to = to;
        this.from = from;
    } 

public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public String getRecipient() {
        return to;
    }
}
```
3. Buatlah kelas EmailApp dengan atribut inbox dan sent sebagai ArrayList yang menyimpan email masuk dan terkirim. Buat juga atribut scanner sebagai objek Scanner untuk membaca input pengguna.


```
class EmailApp {
    private ArrayList<Email> inbox;
    private ArrayList<Email> sent;
    private Scanner scanner;
```
4. Implementasikan konstruktor pada kelas EmailApp untuk menginisialisasi atribut inbox, sent, dan scanner.


```
public EmailApp() {
        inbox = new ArrayList<>();
        sent = new ArrayList<>();
        scanner = new Scanner(System.in);
    }
```
5. Implementasikan metode menu() untuk menampilkan menu aplikasi dan menerima input pengguna.


```
public void menu() {
        while (true) {
            System.out.println("===========GmailApp==========");
            System.out.println("=============================");
            System.out.println("1. Tulis Email baru");
            System.out.println("2. Menampilkan Email masuk");
            System.out.println("3. Menampilkan Email terkirim");
            System.out.println("4. Menghapus Email");
            System.out.println("5. Mencari Email");
            System.out.println("6. Keluar");

            int choice = scanner.nextInt();
            scanner.nextLine();

            if (choice == 1) {
                MengirimEmail();
            } else if (choice == 2) {
                MenampilkanEmailMasuk();
            } else if (choice == 3) {
                MenampilkanEmailTerkirim();
            } else if (choice == 4) {
                MenghapusEmail();
            } else if (choice == 5) {
                MencariEmail();
            } else if (choice == 6) {
                System.out.println("Terima kasih telah menggunakan aplikasi email kami.");
                break;
            } else {
                System.out.println("Pilihan tidak valid.");
            }
        }
    }
```
6. Implementasikan metode MengirimEmail() untuk meminta input pengguna untuk mengirim email baru. Buat objek Email baru dan tambahkan ke inbox dan sent.


```
private void MengirimEmail() {
        System.out.print("Alamat email penerima: ");
        String to = scanner.nextLine();
        String from = getEmailFromUsername(loggedInUsername);
        System.out.print("Subjek: ");
        String subject = scanner.nextLine();
        System.out.print("Isi email: ");
        String body = scanner.nextLine();

        Email email = new Email(to, from, subject, body);
        sent.add(email);
        inbox.add(email);

        System.out.println("Email berhasil dikirim.");
    }
```
7. Implementasikan metode MenampilkanEmailMasuk() untuk menampilkan email yang ada dalam inbox. Jika inbox kosong, tampilkan pesan bahwa tidak ada email masuk.


```
private void MenampilkanEmailMasuk() {
        if (inbox.size() == 0) {
            System.out.println("Tidak ada email masuk.");
        } else {
            for (Email email : inbox) {
                System.out.println("Dari: " + email.getFrom());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }
```
8. Implementasikan metode MenampilkanEmailTerkirim() untuk menampilkan email yang ada dalam sent. Jika sent kosong, tampilkan pesan bahwa tidak ada email terkirim.


```
private void MenampilkanEmailTerkirim() {
        if (sent.size() == 0) {
            System.out.println("Tidak ada email terkirim.");
        } else {
            for (Email email : sent) {
                System.out.println("Ke: " + email.getTo());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }
```
9. Implementasikan metode MenghapusEmail() untuk menghapus semua email dalam inbox dan sent.


```
private void MenghapusEmail() {
        System.out.println("Menghapus semua email masuk dan terkirim...");
        inbox.clear();
        sent.clear();
        System.out.println("Semua email masuk dan terkirim berhasil dihapus.");
    }
```
10. Implementasikan metode MencariEmail() untuk mencari email berdasarkan subjek yang dimasukkan pengguna. Jika ditemukan, tampilkan informasi email. Jika tidak ditemukan, tampilkan pesan bahwa email tidak ditemukan.


```
private void MencariEmail() {
        System.out.print("Masukkan subjek email yang ingin dicari: ");
        String searchSubject = scanner.nextLine();
        boolean found = false;
    
        System.out.println("Hasil pencarian berdasarkan subjek:");
        for (Email email : inbox) {
            if (email.getSubject().equalsIgnoreCase(searchSubject)) {
                System.out.println("Dari: " + email.getFrom());
                System.out.println("Ke: " + email.getTo());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Tidak ditemukan email dengan subjek tersebut.");
        }
    }
}
```
11. `getEmailFromUsername` berfungsi untuk menampilkan alamat gmail pengirim 

```
private String getEmailFromUsername(String username) {
        if (username.equals("Ardian")) {
            return "ardmlkm@gmail.com";
        }
        return null;
    }
```


12. Buatlah kelas Emaill sebagai kelas utama yang berisi metode main. Di dalam metode main, buat objek EmailApp dan panggil metode menu() untuk menjalankan aplikasi.


```
public class Emaill {
public static void main(String[] args) {
EmailApp emailApp = new EmailApp();
emailApp.menu();
}
}
```
Algoritma ini mengizinkan pengguna untuk mengirim email, melihat email masuk dan terkirim, menghapus semua email, dan mencari email berdasarkan subjek. Pengguna akan terus dihadapkan dengan menu hingga memilih untuk keluar dari aplikasi.

# 3. Mampu menjelaskan konsep dasar OOP

Konsep dasar OOP (Object-Oriented Programming) adalah sebuah pemrograman yang fokus pada objek atau entitas dalam program yang memiliki atribut dan perilaku tertentu.


1. **Objek**: Objek adalah instansi dari sebuah kelas. Objek memiliki atribut (data) yang mewakili keadaan objek dan metode (fungsi) yang mewakili perilaku objek. Contoh objek dalam kehidupan sehari-hari adalah mobil, buku, atau manusia.


2. **Kelas**: Kelas adalah blueprint atau template yang mendefinisikan struktur dan perilaku objek.


3. **Inheritance (Pewarisan)**: Inheritance memungkinkan kelas baru (kelas turunan atau subclass) untuk mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass). Hal ini memungkinkan penggunaan kembali kode dan membangun hierarki kelas.


4. **Encapsulation (Enkapsulasi)**: Enkapsulasi menggabungkan data (atribut) dan metode (fungsi) terkait ke dalam satu unit yang disebut objek. Enkapsulasi membatasi akses langsung ke data objek dan mendorong penggunaan metode untuk berinteraksi dengan objek tersebut.


5. **Abstraction (Abstraksi)**: Abstraksi memungkinkan penyembunyian detail internal suatu objek dan hanya mengekspos fitur penting atau antarmuka yang relevan. Abstraksi membantu dalam memfokuskan pada penggunaan objek tanpa perlu memahami implementasi internalnya.


4. **Polymorphism (Polimorfisme)**: Polimorfisme memungkinkan objek dengan jenis yang berbeda untuk merespons metode yang sama dengan cara yang berbeda. Dengan polimorfisme, kita dapat menggunakan objek dari kelas yang berbeda dengan cara yang seragam, meningkatkan fleksibilitas dan modularitas program.


Keempat pilar ini membantu dalam mengorganisir dan membangun program yang lebih fleksibel dan mudah diubah atau dikembangkan kembali.

# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

- Encapsulation adalah konsep dalam Pemrograman Berorientasi Objek yang memungkinkan penyembunyian detail implementasi dari objek, sehingga hanya method-method tertentu yang dapat mengakses dan memanipulasi data dalam objek tersebut. Dengan menggunakan encapsulation, kita dapat memperbaiki keamanan dan modularitas aplikasi serta membuatnya lebih mudah untuk dimodifikasi dan diperbaiki di masa depan.

- abstract class Message {

```
protected String subject;
protected String body;
```
atribut  `subject` dan `body` dalam kelas Message bersifat protected. Ini memungkinkan atribut tersebut diakses oleh kelas yang mewarisi Message, yaitu kelas turunannya Email.
```
abstract class Message {
    protected String subject;
    protected String body;

    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public abstract String getRecipient();
}
```
# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

```
abstract class Message {
    protected String subject;
    protected String body;

    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public abstract String getRecipient();
}
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Message yang memiliki method abstract `getRecipient()`. Method ini harus diimplementasikan di kelas turunan yaitu kelas Email. 

Ini memungkinkan kelas Message untuk menjadi abstrak dan hanya memberikan kerangka kerja umum untuk kelas-kelas turunannya.

```
class Email extends Message {
    private String to;
    private String from;

    public Email(String to, String from, String subject, String body) {
        super(subject, body);
        this.to = to;
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public String getRecipient() {
        return to;
    }
}
```
Dengan menggunakan kelas abstrak dan method abstract, kita dapat mengabstraksi fungsionalitas dan memastikan bahwa kelas turunan mengimplementasikan method-method yang diperlukan dengan cara yang sesuai. Ini adalah salah satu contoh penggunaan abstraction

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

- Inheritance (Pewarisan):

```
class Email extends Message {
    private String to;
    private String from;

    public Email(String to, String from, String subject, String body) {
        super(subject, body);
        this.to = to;
        this.from = from;
    }
//....
```
Pada kodingan di atas, penggunaan Inheritance terlihat pada deklarasi kelas turunan yaitu kelas Email dengan menggunakan keyword `"extends"`, untuk mewarisi sifat-sifat dari kelas induk yaitu kelas abstrak Message. Dengan demikian, kelas turunan memiliki akses ke atribut dan method yang ada di kelas induknya.

- Polymorphism (Polimorfisme):

Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method `override` pada kelas Email. Method `getRecipient()` pada kelas Message merupakan method abstract yang harus diimplementasikan di kelas turunan, yaitu kelas Email.

```
class Email extends Message {
    private String to;
    private String from;

    public Email(String to, String from, String subject, String body) {
        super(subject, body);
        this.to = to;
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public String getRecipient() {
        return to;
    }
}
```
Kelas Email mengoverride method `getRecipient()` dari kelas Message dengan implementasi masing-masing. Dengan menggunakan konsep polymorphism, kita dapat memperlakukan objek dari kelas turunan sebagai objek dari kelas induk, sehingga memudahkan dalam memanipulasi objek-objek tersebut secara umum.

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

- Identifikasi entitas utama:

User: Representasi pengguna Gmail.
Email: Representasi pesan email dengan informasi pengirim, penerima, subjek, dan isi email.

- Identifikasi kelas-kelas yang sesuai:

Message (abstract class): Kelas abstrak yang merepresentasikan pesan umum dengan atribut subjek dan isi.
Email (subclass of Message): Kelas yang mewarisi dari kelas Message dan menambahkan atribut penerima (to) dan pengirim (from).
EmailApp: Kelas yang mengelola aplikasi Gmail, termasuk pengelolaan kotak masuk (inbox), email terkirim (sent), login, pengiriman email, dan tampilan email.

- Implementasikan metode-metode dan fitur-fitur dalam kelas-kelas:

Kelas Message:

Memiliki konstruktor untuk menginisialisasi subjek dan isi pesan.
Memiliki metode untuk mengambil subjek dan isi pesan.
Memiliki metode abstrak getRecipient() yang perlu diimplementasikan oleh kelas turunannya.

```
abstract class Message {
    protected String subject;
    protected String body;

    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public abstract String getRecipient();
}
```



Kelas Email:

Mewarisi dari kelas Message.
Memiliki atribut tambahan yaitu penerima (to) dan pengirim (from).
Mengimplementasikan metode abstrak getRecipient() untuk mengembalikan penerima email.
Memiliki metode getter untuk mengakses penerima dan pengirim email.

```
class Email extends Message {
    private String to;
    private String from;

    public Email(String to, String from, String subject, String body) {
        super(subject, body);
        this.to = to;
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public String getRecipient() {
        return to;
    }
}
```



Kelas EmailApp:

Mengelola kotak masuk (inbox) dan email terkirim (sent) menggunakan ArrayList.
Menggunakan objek Scanner untuk input pengguna.
Melakukan login dengan memvalidasi username dan password.
Menampilkan menu dan memungkinkan pengguna untuk memilih opsi yang diinginkan.
Implementasi metode untuk mengirim email, menampilkan email masuk, email terkirim, menghapus email, mencari email, dan mengonversi username menjadi alamat email.

```
class EmailApp {
    private ArrayList<Email> inbox;
    private ArrayList<Email> sent;
    private Scanner scanner;
    private boolean loggedIn;
    private String loggedInUsername;

    public EmailApp() {
        inbox = new ArrayList<>();
        sent = new ArrayList<>();
        scanner = new Scanner(System.in);
        loggedIn = false;
        loggedInUsername = "";
    }

    public void menu() {
        while (true) {
            if (!loggedIn) {
                login();
                continue;
            }

            System.out.println("===========GmailApp==========");
            System.out.println("=============================");
            System.out.println("1. Tulis Email baru");
            System.out.println("2. Menampilkan Email masuk");
            System.out.println("3. Menampilkan Email terkirim");
            System.out.println("4. Menghapus Email");
            System.out.println("5. Mencari Email");
            System.out.println("6. Keluar");

            int choice = scanner.nextInt();
            scanner.nextLine();

            if (choice == 1) {
                MengirimEmail();
            } else if (choice == 2) {
                MenampilkanEmailMasuk();
            } else if (choice == 3) {
                MenampilkanEmailTerkirim();
            } else if (choice == 4) {
                MenghapusEmail();
            } else if (choice == 5) {
                MencariEmail();
            } else if (choice == 6) {
                System.out.println("Terima kasih telah menggunakan aplikasi email kami.");
                break;
            } else {
                System.out.println("Pilihan tidak valid.");
            }
        }
    }

    private void login() {
        while (!loggedIn) {
            System.out.println("Silakan login terlebih dahulu.");
            System.out.print("Username: ");
            String username = scanner.nextLine();
            System.out.print("Password: ");
            String password = scanner.nextLine();

            if (username.equals("Ardian") && password.equals("12345")) {
                loggedIn = true;
                loggedInUsername = username;
                System.out.println("Login berhasil. Selamat datang, " + username + "!");
            } else {
                System.out.println("Login gagal. Periksa kembali username dan password Anda.");
            }
        }
    }

    private void MengirimEmail() {
        System.out.print("Alamat email penerima: ");
        String to = scanner.nextLine();
        String from = getEmailFromUsername(loggedInUsername);
        System.out.print("Subjek: ");
        String subject = scanner.nextLine();
        System.out.print("Isi email: ");
        String body = scanner.nextLine();

        Email email = new Email(to, from, subject, body);
        sent.add(email);
        inbox.add(email);

        System.out.println("Email berhasil dikirim.");
    }

    private void MenampilkanEmailMasuk() {
        if (inbox.size() == 0) {
            System.out.println("Tidak ada email masuk.");
        } else {
            for (Email email : inbox) {
                System.out.println("Dari: " + email.getFrom());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }

    private void MenampilkanEmailTerkirim() {
        if (sent.size() == 0) {
            System.out.println("Tidak ada email terkirim.");
        } else {
            for (Email email : sent) {
                System.out.println("Ke: " + email.getTo());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
            }
        }
    }

    private void MenghapusEmail() {
        System.out.println("Menghapus semua email masuk dan terkirim...");
        inbox.clear();
        sent.clear();
        System.out.println("Semua email masuk dan terkirim berhasil dihapus.");
    }

    private void MencariEmail() {
        System.out.print("Masukkan subjek email yang ingin dicari: ");
        String searchSubject = scanner.nextLine();
        boolean found = false;

        System.out.println("Hasil pencarian berdasarkan subjek:");
        for (Email email : inbox) {
            if (email.getSubject().equalsIgnoreCase(searchSubject)) {
                System.out.println("Dari: " + email.getFrom());
                System.out.println("Ke: " + email.getTo());
                System.out.println("Subjek: " + email.getSubject());
                System.out.println("Isi: " + email.getBody());
                System.out.println();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Tidak ditemukan email dengan subjek tersebut.");
        }
    }

    private String getEmailFromUsername(String username) {
        if (username.equals("Ardian")) {
            return "ardmlkm@gmail.com";
        }
        return null;
    }
}
```

- Pemanggilan program utama:

Pada method main, objek EmailApp dibuat dan metode menu() dipanggil untuk memulai aplikasi Gmail. untuk memastikan bahwa fungsionalitas yang diharapkan berjalan dengan baik dan sesuai dengan kebutuhan bisnis.

```
public class Emaill {
    public static void main(String[] args) {
        EmailApp emailApp = new EmailApp();
        emailApp.menu();
    }
}
```




# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

Class diagram menggunakan mermaid

```mermaid
classDiagram
    class Message {
        # subject: String
        # body: String
        + Message(subject: String, body: String)
        + getSubject(): String
        + getBody(): String
        + abstract getRecipient(): String
    }

    class Email {
        - to: String
        - from: String
        + Email(to: String, from: String, subject: String, body: String)
        + getTo(): String
        + getFrom(): String
        + getRecipient(): String
    }

    class EmailApp {
        - inbox: ArrayList<Email>
        - sent: ArrayList<Email>
        - scanner: Scanner
        - loggedIn: boolean
        - loggedInUsername: String
        + EmailApp()
        + menu()
        + login()
        + MengirimEmail()
        + MenampilkanEmailMasuk()
        + MenampilkanEmailTerkirim()
        + MenghapusEmail()
        + MencariEmail()
        + getEmailFromUsername(username: String): String
    }

    EmailApp o-- Email
    EmailApp o-- Scanner
    EmailApp --> ArrayList
    Email --|> Message

```

Use Case table
| Use Case Gmail | Nilai Prioritas |
| ------ | ------ |
|1. Mengirim Email :<br> saya menggunakan method MengirimEmail() yang berfungsi untuk mengirimkan pesan dengan menentukan tujuan pesan dikirim kepada siapa, dari siapa, subject apa, dan isinya apa | 100
|2. Menerima email masuk : <br> Terdapat method MenampilkanEmailMasuk() yang dapat menampilkan email yang masuk kepada email kita dengan menampilkan dari, subjek, isi | 100
|3. Menampilkan email keluar : <br> Method MenampilkanEmailTerkirim() berfungsi untuk menampilkan email yang sudah terkirim beserta dengan subjek, dan isi dari pesan yang dikirim | 100
|4. Mencari email : <br> Method MencariEmail() berfungsi untuk mencari email yang berdasarkan subjek dari email terkirim | 100
|5. Membalas dan meneruskan email | 90
|6. Mengatur email | 90
|7. Mengunduh email | 90
|8. Menghapus email : <br> Method MenghapusEmail() berfungsi untuk menghapus email terkirim dan email masuk  | 100
|9. Mencari email | 80
|10. Mendraf email | 70
|11. Mengakses email dari berbagai perangkat | 60
|12. Menghapus spam dan email tidak diinginkan | 70
|13. Menandai email sebagai penting atau bintang |70
|14. Mengelola folder dan label email | 60
|15. Mengatur filter email | 70
|16. Mengatur tanda tangan email | 60
|17. Mengatur notifikasi email | 60
|18. Mengatur pengaturan keamanan dan privasi | 50
|19. Mengatur pengaturan bahasa dan tampilan | 50
|20. Mengatur kapasitas penyimpanan email | 60
|21. Melakukan pencadangan email | 50
|22. Menggabungkan kotak masuk | 40
|23. Melakukan impor email dari akun lain | 40

# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

https://youtu.be/HKsIeZZuptg 

# 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

1. Menu mengirim email

![dokumentasi jawaban](dokumentasi jawaban/mengirim.png)

2. Menu menampilkan email masuk

![dokumentasi jawaban](dokumentasi jawaban/menampilkanpesanmasuk.png)

3. Menu menampilkan email terkirim

![dokumentasi jawaban](dokumentasi jawaban/menampilkanpesanterkirim.png)

4. Menu mencari email berdasarkan subjek

![dokumentasi jawaban](dokumentasi jawaban/mencaripesan.png)

5. Menu menghapus semua email masuk dan keluar

![dokumentasi jawaban](dokumentasi jawaban/menghapuspesan.png)

