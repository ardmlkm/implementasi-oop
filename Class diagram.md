# 1. Apa itu Class Diagram?

Diagram kelas adalah salah satu jenis diagram struktur pada UML yang menggambarkan dengan jelas struktur serta deskripsi class, atribut, metode, dan hubungan dari setiap objek. Ia bersifat statis, dalam artian diagram kelas bukan menjelaskan apa yang terjadi jika kelas-kelasnya berhubungan, melainkan menjelaskan hubungan apa yang terjadi.

Diagram kelas ini sesuai jika diimplementasikan ke proyek yang menggunakan konsep object-oriented karena gambaran dari class diagram cukup mudah untuk digunakan.

# 2. Fungsi Class Diagram

Agar lebih memahami tentang class diagram, sebaiknya kita mengetahui tentang apa fungsi dari Class diagram itu sendiri dalam sebuah sistem. Jadi, beberapa fungsi class diagram adalah sebagai berikut:


- Dapat meningkatkan pemahaman mengenai gambaran umum atau suatu skema dari program yang dibuat
- Dapat menunjukan struktur sebuah sistem dengan sangat jelas
- Dapat memberikan gambaran tentang perangkat lunak dan relasi-relasi yang ada di dalamnya
- Dapat menjadi bahan analisis bisnis, serta dapat digunakan untuk model sistem yang akan dibuat dari sisi bisnis

# 3. Komponen Class Diagram

![dokumentasi jawaban](dokumentasi jawaban/komponendiagramclass.png)


- Komponen Atas
Bagian ini berisikan nama kelas yang selalu diperlukan, baik dalam pengklasifikasian atau objek. Setiap kelas juga pasti memiliki nama yang berbeda-beda dengan kelas lain.

- Komponen Tengah
Komponen tengah berisikan atribut kelas yang digunakan untuk mendeskripsikan kualitas kelas. Dimana ini hanya diperlukan saat menggambarkan kelas tertentu. Singkatnya, atribut dapat menjelaskan rentang nilai sifat tersebut.

- Komponen Bawah
Bagian bawah dalam suatu kelas menyertakan operasi kelas atau metode yang ditampilkan dalam format daftar.

# 4. Hubungan Antar Class Diagram

- Association (Asosiasi): Relasi hubungan asosiasi menggambarkan keterkaitan antara dua class yang terhubung melalui suatu hubungan. Ini adalah relasi umum yang menunjukkan bahwa ada keterkaitan antara objek-objek dalam class tersebut. Contoh: Hubungan antara class "Customer" dan class "Order" dalam sistem penjualan.

- Aggregation (Agregasi): Relasi hubungan agregasi menggambarkan hubungan "has-a" antara class yang berperan sebagai container (whole) dan class yang berperan sebagai bagian dari container (part). Bagian dapat ada secara independen dari container. Contoh: Hubungan antara class "Department" (container) dan class "Employee" (bagian) dalam sistem manajemen organisasi.

- Composition (Komposisi): Relasi hubungan komposisi juga menggambarkan hubungan "has-a" antara class container dan class bagian. Namun, perbedaan utama dengan agregasi adalah bahwa bagian merupakan bagian integral dari container dan tidak dapat ada secara independen. Contoh: Hubungan antara class "Car" (container) dan class "Engine" (bagian) dalam sistem otomotif.

- Generalization (Generalisasi): Relasi hubungan generalisasi menggambarkan pewarisan atau hierarki class. Superclass (class yang lebih umum) menjadi parent class dari subclass (class yang lebih khusus). Subclass mewarisi atribut dan metode dari superclass dan dapat menambahkan atribut dan metode tambahan yang unik. Contoh: Hubungan antara superclass "Animal" dan subclass "Cat" serta "Dog" dalam sistem hewan peliharaan.

- Dependency (Ketergantungan): Relasi hubungan dependency menggambarkan ketergantungan antara class. Perubahan pada class yang menjadi ketergantungan dapat mempengaruhi class yang bergantung, tetapi tidak ada hubungan struktural permanen antara keduanya. Contoh: Hubungan antara class "Order" dan class "Payment" dalam sistem pembelian.

- Realization (Realisasi): Relasi hubungan realisasi menggambarkan implementasi sebuah interface oleh sebuah class. Class yang mengimplementasikan interface harus menyediakan implementasi untuk metode-metode yang didefinisikan oleh interface tersebut. Contoh: Hubungan antara class "Circle" yang mengimplementasikan interface "Shape" dalam sistem penggambaran geometri.

# 5. Notasi Modifier pada Class Diagram

| Tanda |  Deskripsi |
| ------ | ------ |
| +       | Modifier  Public       |
| -       | Modifier Private       |
| #       | Modifier Protected       |

# 6. Notasi Hubungan Antar Class Diagram

```mermaid
classDiagram
classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)
```
# 7. Contoh Class Diagram

```mermaid
classDiagram
    class Message {
        # subject: String
        # body: String
        + Message(subject: String, body: String)
        + getSubject(): String
        + getBody(): String
        + abstract getRecipient(): String
    }

    class Email {
        - to: String
        - from: String
        + Email(to: String, from: String, subject: String, body: String)
        + getTo(): String
        + getFrom(): String
        + getRecipient(): String
    }

    class EmailApp {
        - inbox: ArrayList<Email>
        - sent: ArrayList<Email>
        - scanner: Scanner
        - loggedIn: boolean
        - loggedInUsername: String
        + EmailApp()
        + menu()
        + login()
        + MengirimEmail()
        + MenampilkanEmailMasuk()
        + MenampilkanEmailTerkirim()
        + MenghapusEmail()
        + MencariEmail()
        + getEmailFromUsername(username: String): String
    }

    EmailApp o-- Email
    EmailApp o-- Scanner
    EmailApp --> ArrayList
    Email --|> Message

```
# 8. Penjelasan Class Diagram

# Referensi
https://pccontrol.wordpress.com/2013/01/21/pengetahuan-dasar-dan-contoh-diagram-kelas-class-diagram/

https://www.dicoding.com/blog/memahami-class-diagram-lebih-baik/

https://accurate.id/teknologi/class-diagram/



